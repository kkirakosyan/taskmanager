package com.test.task.taskManager.service;

import com.test.task.taskManager.model.User;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.event.ApplicationReadyEvent;
import org.springframework.context.event.EventListener;
import org.springframework.stereotype.Service;

@Service
public class InitApplicationService {

    private static final Logger LOGGER = LoggerFactory.getLogger(InitApplicationService.class);

    private TaskManagerService taskManagerService;

    @EventListener(ApplicationReadyEvent.class)
    public void initializeTestData() {
        LOGGER.info("Initialize test data");

        taskManagerService.createUser(new User("user1","firstName1","lastName1"));
        taskManagerService.createUser(new User("user2","firstName2","lastName2"));
        taskManagerService.createUser(new User("user3","firstName3","lastName3"));

        LOGGER.info("Initialization completed");
    }

    @Autowired
    public void setTaskManagerService(TaskManagerService taskManagerService) {
        this.taskManagerService = taskManagerService;
    }

}