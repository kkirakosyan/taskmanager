package com.test.task.taskManager.service;


import com.test.task.taskManager.model.Comment;
import com.test.task.taskManager.model.Notification;
import com.test.task.taskManager.model.Task;
import com.test.task.taskManager.model.User;
import com.test.task.taskManager.repository.CommentRepository;
import com.test.task.taskManager.repository.NotificationRepository;
import com.test.task.taskManager.repository.TaskRepository;
import com.test.task.taskManager.repository.UserRepository;
import com.test.task.taskManager.util.NotificationType;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class TaskManagerService {

    private CommentRepository commentRepository;
    private NotificationRepository notificationRepository;
    private TaskRepository taskRepository;
    private UserRepository userRepository;


    public User createUser(User user) {
        return userRepository.save(user);
    }

    public List<User> findAllUser() {
        return userRepository.findAll();
    }

    public void changeAssignee(Task task, User targetAssignee){
        User oldAssignee = task.getAssignee();
        oldAssignee.removeTask(task);
        task.setAssignee(targetAssignee);

        Notification notificationForAssignee = new Notification();
        notificationForAssignee.setCreationDate(LocalDateTime.now());
        notificationForAssignee.setType(NotificationType.ASSIGNEE_CHANGED);
        notificationForAssignee.setUser(targetAssignee);
        notificationForAssignee.setTask(task);

        targetAssignee.getNotifications().add(notificationForAssignee);
        targetAssignee.addTask(task);
        userRepository.save(targetAssignee);
        userRepository.save(oldAssignee);
    }

    public List<Notification> findNotificationsForUser(User user) {
        return user.getNotifications();
    }

    public List<Comment> findCommentsForUser(Task task){
        return task.getComments();
    }

    @Autowired
    public void setCommentRepository(CommentRepository commentRepository) {
        this.commentRepository = commentRepository;
    }

    @Autowired
    public void setNotificationRepository(NotificationRepository notificationRepository) {
        this.notificationRepository = notificationRepository;
    }

    @Autowired
    public void setTaskRepository(TaskRepository taskRepository) {
        this.taskRepository = taskRepository;
    }

    @Autowired
    public void setUserRepository(UserRepository userRepository) {
        this.userRepository = userRepository;
    }

    //    User userReporter = new User();
    //    Task task1 = new Task();
    //        task1.setReporter(userReporter);
    //    Task task2 = new Task();
    //        task2.setReporter(userReporter);
    //    Task task3 = new Task();
    //        task3.setReporter(userReporter);
    //
    //    User userAssignee = new User();
    //        userAssignee.addTask(task1);
    //        userAssignee.addTask(task2);
    //        userAssignee.addTask(task3);
    //
    //        taskManagerRepository.save(userReporter);
    //        System.out.println("______________________________________");
    //
    //
    //        taskManagerRepository.save(userAssignee);
}
