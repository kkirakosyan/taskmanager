package com.test.task.taskManager.controller;


import com.test.task.taskManager.model.User;
import com.test.task.taskManager.service.TaskManagerService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import java.util.List;

@Controller
@RequestMapping("/taskManager")
public class TaskMangerController {


    private TaskManagerService taskManagerService;

    @RequestMapping(value = "/usersList", method = RequestMethod.GET)
    public String findAllUser(Model model) {

        List<User> users = taskManagerService.findAllUser();
        model.addAttribute("users", users);
        return "userList";
    }


    @Autowired
    public void setTaskManagerService(TaskManagerService taskManagerService) {
        this.taskManagerService = taskManagerService;
    }
}
