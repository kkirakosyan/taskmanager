package com.test.task.taskManager.util;

public enum TaskType {
    BUG,
    STORY,
    EPIC,
    TASK
}
