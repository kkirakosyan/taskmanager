package com.test.task.taskManager.util;

public enum TaskStatusType {
    OPEN,
    REOPENED,
    IN_PROGRESS,
    DONE
}
