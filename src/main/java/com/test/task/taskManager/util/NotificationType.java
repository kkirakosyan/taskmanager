package com.test.task.taskManager.util;

public enum NotificationType {
    STATUS_CHANGED,
    COMMENT,
    ASSIGNEE_CHANGED
}
