package com.test.task.taskManager.util;

public enum SeverityType {
    MAJOR,
    MINOR,
    TRIVIAL,
    CRITICAL,
    BLOCKER
}
